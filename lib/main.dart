import 'package:flutter/material.dart';

// La funcion main es el punto de partida para todas nuestras aplicaciones en flutter
void main() {
  runApp(MaterialApp(
    home: Scaffold(
      backgroundColor: Colors.blueGrey,
      appBar: AppBar(
        centerTitle: true,
        title: Text('I Am Rich'),
        backgroundColor: Colors.blueGrey[900],
      ),
      body: Center(
        child: Image(
          height: 150.0,
          width: 150.0,
          image: AssetImage('assets/images/diamond.png'),
        ),
      ),
    ),
  ));
}
